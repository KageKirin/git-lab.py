#!/usr/local/bin/python3

import os
import sys
import re
import string
from pathlib import Path
from pygit2 import Repository
from gitlab import Gitlab
import giturlparse
import argparse

repo = Repository('.')

def glGetGroupId(gl, name):
    try:
        return gl.groups.search(name)[0].id
    except:
        print(name, 'is not an organization. error')
        return None

def glGetUserId(gl, name):
    try:
        return gl.users.list(username=name)[0].id
    except:
        print(name, 'is not an user. error')
        return None

def glGetOwnerId(gl, namespace):
    repoOwner = str(Path(str(Path(namespace).parent).replace(':', '/')).stem)
    groupId = glGetGroupId(gl, repoOwner)
    if groupId:
        return groupId, False

    userId = glGetUserId(gl, repoOwner)
    if userId and userId == gl.user.id:
        print(repoOwner, "is authorized user")
        return userId, True

    print("only authorized user can work in his profile")
    return None, None

def glGetUserRepoNames(gl, userId):
    user = gl.users.get(userId)
    return [p.name for p in user.projects.list()]

def glGetGroupRepoNames(gl, groupId):
    group = gl.groups.get(groupId)
    return [p.name for p in group.projects.list()]


def glGetRepo(gl, owner, repoName):
    return gl.projects.get('{}/{}'.format(owner, repoName))


###----

def createmergerequest(args, gl_repo):
    parser = argparse.ArgumentParser()
    parser.add_argument('--into', help='base branch to merge into', type=str, default="master")
    parser.add_argument('--head', help='branch to merge', type=str, default=repo.head.shorthand)
    parser.add_argument('--issue', help='issue to reference', type=str, default='')
    parser.add_argument('-t', '--title', help='title', type=str, default='merge {}'.format(repo.head.shorthand))
    parser.add_argument('-m', '--message', help='message', type=str, default='merging {}'.format(repo.head.shorthand))
    subargs = parser.parse_args(args.tail)

    gl_pr = gl_repo.mergerequests.create({
        'title': subargs.title,
        'description': subargs.message,
        'target_branch': subargs.into,
        'source_branch': subargs.head,
    })
    assert gl_pr, "could not create merge request"
    print('created merge request:', gl_pr.web_url)

def createpullrequest(args, gl_repo):
    print("a pull request is called 'merge request' in gitlab terms")
    return createmergerequest(args, gl_repo)

###----

def main(args):
    remo = repo.remotes[args.remote]
    url = giturlparse.parse(remo.url)

    tokens = []
    if args.token != '':
        tokens.append(args.token)
    tokens.extend(list(map(lambda t: t.value, filter(lambda h: h.name == 'lab.'+url.resource+'.token', repo.config.__iter__()))))
    url_http = 'https://{}/{}'.format(url.resource, url.pathname)
    url_api = 'https://{}'.format(url.resource)

    gl = None
    for t in tokens:
        # Gitlab with custom hostname
        gl = Gitlab(url_api, private_token=t)

        if gl and gl.auth():
            break
    assert gl, "could not access remote repo at {}".format(url_http)
    gl_repo = glGetRepo(gl, url.owner, url.name)
    assert gl_repo, "could retrieve remote repo at {}".format(url_http)

    action = None
    action_name = re.sub(r'\W+', '', args.action)
    if hasattr(sys.modules[__name__], action_name):
        action = getattr(sys.modules[__name__], action_name)
    assert action, "{} is not a defined action".format(args.action)
    action(args, gl_repo)

###----

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-t', '--token', help='override user token', type=str, default="")
    parser.add_argument('remote', help='git remote to use', type=str)
    parser.add_argument('action', help='action to take on remote', type=str)
    parser.add_argument('tail', nargs=argparse.REMAINDER)
    args = parser.parse_args()
    main(args)

###----
