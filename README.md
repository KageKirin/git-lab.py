# git-lab.py

A small git extension for GitLab,
written in Python3 and relying on GitLab API.

## Install

clone and add this to your path.
`pip install -r requirements.txt` as well

## Usage

### create a merge request

```
git lab [--token <gitlab-api-token>] <remote> create-merge-request [--into <master>] [--head <current branch>] [--title <title>] [--message <message>]
```

### more functionality to be added over time

## Configuration

To avoid having to pass the `--token` argument at every invokation,
you can set up the configuration (either global or local)
to contain the following entries:

```
[lab "gitlab domain"]
	token = your token
```

The tool will to try to match the correct configuration entries with the given remote

Example:

```
[lab "gitlab.com"]
	token = token123token123
[lab "gitlab.enterprise.tld"]
	token = token123token123
```

## Example

```
git lab origin create-merge-request --into master --head feature/cookies -t "merging cookies" -m "hmm! cookies!!"
```
